#! /bin/bash

# Variables
dir="$(dirname "$0")"

# Inclusions des fichiers de fonctions
. functions/tuxedo_fan_control
. functions/tuxedo_keyboard

# Installe le controleur de vitesse des ventilateurs
tuxedoFanControl
# Driver clavier
tuxedoKeyboard

# Update && Upgrade
echo -e ${CYAN}"Mise à jour du système..."${RESETCOLOR}
apt update
apt upgrade -y
apt full-upgrade -y
apt autoremove -y
apt autoclean