#! /bin/bash

# Variables
dir="$(dirname "$0")"
# Inclusions des fichiers de fonctions
. functions/tuxedo_kernel_amd

# Couleur pour la console
CYAN="\\033[1;96m" 
PINK="\e[38;5;198m"
RED="\\033[1;31m"
GREEN="\e[038;5;82m"
RESETCOLOR="\\033[0;0m"

# Verification des permissions root
if [ $(whoami) != 'root' ];then
    echo -e  ${RED}"Ce script a besoin des permissions adminisrtateur (sudo), veuillez le lancer de la manière suivant : sudo ./first_script.sh"${RESETCOLOR}
    exit 0
fi

# Verification de la connexion internet
echo -en ${CYAN}"Contrôle de la connexion internet..."${RESETCOLOR}
wget -q --spider http://ch.archive.ubuntu.com/ubuntu/dists/
if [ $? -eq 0 ]; then
    echo -e ${CYAN}"\e[0K\rContrôle de la connexion internet"${GREEN}"    [OK]"${RESETCOLOR} # \e[0K\r = retour en début de ligne
else
    echo -e ${CYAN}"\e[0K\rContrôle de la connexion internet"${RED}"    [KO]"${RESETCOLOR}
    echo -e ${RED}"\nVeuillez vérifier votre connexion internet"${RESETCOLOR}
    exit 0
fi

# Active Canonical ppa + update des dépôts
echo -e ${CYAN}"Activation du dépôt canonical..."${RESETCOLOR}
sed -i -e '/# deb http:\/\/archive.canonical.com\/ubuntu\ /s/# //' /etc/apt/sources.list
apt update

# Installation des applications favorites de la liste apps_to_install.list
echo -e ${CYAN}"Installation des applications..."${RESETCOLOR}
FAVS="$dir/ressources/apps_to_install.list"
apt install -y --no-install-recommends $(cat $FAVS)
apt --fix-broken install
apt autoremove -y

# installation fonts Microsoft avec paquet .deb car il y a souvent des erreurs avec le paquet Ubuntu
echo -e ${CYAN}"Installation des fonts Microsoft..."${RESETCOLOR}
apt install -y cabextract
dpkg -i $dir/resources/ttf-mscorefonts-installer_3.7_all.deb

# Modification des application du dock
echo -e ${CYAN}"Modification des application du dock..."${RESETCOLOR} 
cp -r $dir/resources/99_launcher.favorites.gschema.override /usr/share/glib-2.0/schemas/99_launcher.favorites.gschema.override
glib-compile-schemas /usr/share/glib-2.0/schemas/

# Installation du script Brother pour l'installation des pilotes d'impression
echo -e ${CYAN}"Installation du script Brother..."${RESETCOLOR}
cp $dir/resources/linux-brprinter-installer-2.2.0-1 /usr/local/bin/
cp $dir/resources/brotherInstall.desktop /usr/share/applications/
cp $dir/resources/printer.png /usr/share/applications/
chmod a+rx /usr/local/bin/linux-brprinter-installer-2.2.0-1

# Modification de grub pour ne pas afficher le menu
echo -e ${CYAN}"Modification de grub (menu)..."${RESETCOLOR}
sed -i '/^GRUB_TIMEOUT=/c\GRUB_TIMEOUT=0' /etc/default/grub
update-grub

# Installation du Kernel de chez Tuxedo
tuxedoKernelAMD 

# Redémarrage du PC
reboot
